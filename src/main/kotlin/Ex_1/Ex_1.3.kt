package Ex_1

import kotlinx.coroutines.*


fun main()= runBlocking {
    println("The main program is started")
    doBackground2()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
suspend fun doBackground2() {
    withContext(Dispatchers.Default) {
        launch {
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}