package Ex_1

import kotlinx.coroutines.*


fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

/*
The main program is started
The main program continues
Background processing started
1s
Background processing finished
1.5s
The main program is finished
 */