import kotlinx.coroutines.*
import java.util.*

val sc=Scanner(System.`in`)
fun main() = runBlocking{
    val randomNumber = 25
    println("You have 10 seconds to guess the secret number...")
    launch {
        guessNumberRandom(randomNumber)
    }
    delay(10000)
    println("The time is up! The secret number was $randomNumber")
}

suspend fun guessNumberRandom(randomNumber: Int){
    var salida = false
    do{
        print("Enter a number:")
        val numberPropose = sc.nextInt()
        if (numberPropose == randomNumber){
            println("You got it!")
            salida = true
        }
        else println("Wrong number!")
    }while (!salida)

}



