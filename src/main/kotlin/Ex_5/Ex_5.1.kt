package Ex_5


import kotlinx.coroutines.*

fun main() {
    runBlocking {
        launch {
            corroutine1_2()
        }
        launch {
            corroutine2_2()
        }
    }
    println("Completed")
}

suspend fun corroutine1_2() {
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }


suspend fun corroutine2_2() {
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }

