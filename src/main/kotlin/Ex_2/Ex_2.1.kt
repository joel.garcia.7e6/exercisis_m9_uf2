package Ex_2

import kotlinx.coroutines.*


fun main(){
    runBlocking{
        launch {
            printHelloWorld2()
        }
    }
    println("Finished!")
}

suspend fun printHelloWorld2() {
    for(i in 1 .. 10){
        println("$i Hello World!")
        delay(100)
    }
}