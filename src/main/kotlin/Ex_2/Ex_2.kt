package Ex_2

import kotlinx.coroutines.*


fun main()= runBlocking{
    printHelloWorld()
    println("Finished!")
}

suspend fun printHelloWorld() {
    withContext(Dispatchers.Default) {
            launch {
                for(i in 1 .. 10){
                println("$i Hello World!")
                delay(100)
            }
        }
    }
}